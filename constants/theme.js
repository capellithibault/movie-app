const COLORS = {
  dark_blue: '#080B18',
  purple: '#1F147A',
  blue_grey: '#172737',
  grey: '#4E5760',
  yellow: '#FDB53E',
  white: '#FFF'
};
const theme = {
  colors: COLORS,
  icon: {
    color: COLORS.white
  },
  text: {
    color: COLORS.white
  }
};

export default theme;
