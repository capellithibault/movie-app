const data = [
  {
    id: 1,
    title: 'Blade Runner',
    subTitle: null,
    genre: 'Action',
    abstract:
      'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque voluptatem adipisci perferendis eligendi, maxime cum reprehenderit placeat cupiditate aliquam amet veritatis dolorem ipsa alias deleniti et quam illum, sed a.',
    rate: {
      press: [3.5, 4, 3],
      spectator: [4, 4.5, 3]
    },
    poster: require('../assets/posters/4f1c91cd5fb8ad3eadaa39e43cd4e93d.png'),
    marked: false
  },
  {
    id: 2,
    title: 'Jurassic Park',
    subTitle: null,
    genre: 'Action',
    abstract:
      'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque voluptatem adipisci perferendis eligendi, maxime cum reprehenderit placeat cupiditate aliquam amet veritatis dolorem ipsa alias deleniti et quam illum, sed a.',
    rate: {
      press: [3.5, 4, 3],
      spectator: [4, 4.5, 3]
    },
    poster: require('../assets/posters/6efff842356fdafda6e8304ebc42d3ec.png'),
    marked: false
  },
  {
    id: 3,
    title: 'Avengers',
    subTitle: 'Endgame',
    genre: 'Action',
    abstract:
      'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque voluptatem adipisci perferendis eligendi, maxime cum reprehenderit placeat cupiditate aliquam amet veritatis dolorem ipsa alias deleniti et quam illum, sed a.',
    rate: {
      press: [3.5, 4, 3],
      spectator: [4, 4.5, 3]
    },
    poster: require('../assets/posters/7b3cf414cc0c8f080c0ed28847aa6588.png'),
    marked: false
  },
  {
    id: 4,
    title: 'Godzilla',
    subTitle: 'Le Roi des Monstres',
    genre: 'Action',
    abstract:
      'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque voluptatem adipisci perferendis eligendi, maxime cum reprehenderit placeat cupiditate aliquam amet veritatis dolorem ipsa alias deleniti et quam illum, sed a.',
    rate: {
      press: [3.5, 4, 3],
      spectator: [4, 4.5, 3]
    },
    poster: require('../assets/posters/7e161b5625b4eb6a8c23c92da81c5657.png'),
    marked: false
  },
  {
    id: 5,
    title: 'Le Hobbit',
    subTitle: 'La Désolation de Smaug',
    genre: 'Aventure',
    abstract:
      'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque voluptatem adipisci perferendis eligendi, maxime cum reprehenderit placeat cupiditate aliquam amet veritatis dolorem ipsa alias deleniti et quam illum, sed a.',
    rate: {
      press: [3.5, 4, 3],
      spectator: [4, 4.5, 3]
    },
    poster: require('../assets/posters/015a36615fe5ba747486b72a4f720c3a.png'),
    marked: false
  },
  {
    id: 6,
    title: 'Star Wars',
    subTitle: "L'Empire Contre Attaque",
    genre: 'Science Fiction',
    abstract:
      'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque voluptatem adipisci perferendis eligendi, maxime cum reprehenderit placeat cupiditate aliquam amet veritatis dolorem ipsa alias deleniti et quam illum, sed a.',
    rate: {
      press: [3.5, 4, 3],
      spectator: [4, 4.5, 3]
    },
    poster: require('../assets/posters/46d2ae37a9222174a0f4658997913c95.png'),
    marked: false
  },
  {
    id: 7,
    title: 'Beetlejuice',
    subTitle: null,
    genre: 'Horreur',
    abstract:
      'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque voluptatem adipisci perferendis eligendi, maxime cum reprehenderit placeat cupiditate aliquam amet veritatis dolorem ipsa alias deleniti et quam illum, sed a.',
    rate: {
      press: [3.5, 4, 3],
      spectator: [4, 4.5, 3]
    },
    poster: require('../assets/posters/60a275e6490656b8fa045d270ca492de.png'),
    marked: false
  },
  {
    id: 8,
    title: 'La Famille Adams',
    subTitle: null,
    genre: 'Fantaisie',
    abstract:
      'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque voluptatem adipisci perferendis eligendi, maxime cum reprehenderit placeat cupiditate aliquam amet veritatis dolorem ipsa alias deleniti et quam illum, sed a.',
    rate: {
      press: [3.5, 4, 3],
      spectator: [4, 4.5, 3]
    },
    poster: require('../assets/posters/74c62d9d9020705fa15f4e443683d877.png'),
    marked: false
  },
  {
    id: 9,
    title: 'La Casa del Papel',
    subTitle: null,
    genre: 'Action',
    abstract:
      'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque voluptatem adipisci perferendis eligendi, maxime cum reprehenderit placeat cupiditate aliquam amet veritatis dolorem ipsa alias deleniti et quam illum, sed a.',
    rate: {
      press: [3.5, 4, 3],
      spectator: [4, 4.5, 3]
    },
    poster: require('../assets/posters/76da0d454d31fdcdb70fcd1b75943165.png'),
    marked: false
  },
  {
    id: 10,
    title: 'Toy Story',
    subTitle: null,
    genre: 'Dessin Animé',
    abstract:
      'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque voluptatem adipisci perferendis eligendi, maxime cum reprehenderit placeat cupiditate aliquam amet veritatis dolorem ipsa alias deleniti et quam illum, sed a.',
    rate: {
      press: [3.5, 4, 3],
      spectator: [4, 4.5, 3]
    },
    poster: require('../assets/posters/87cb0e73c4b9098b215aee61c7253efe.png'),
    marked: false
  },
  {
    id: 11,
    title: 'Kill Bill',
    subTitle: 'Volume 1',
    genre: 'Action',
    abstract:
      'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque voluptatem adipisci perferendis eligendi, maxime cum reprehenderit placeat cupiditate aliquam amet veritatis dolorem ipsa alias deleniti et quam illum, sed a.',
    rate: {
      press: [3.5, 4, 3],
      spectator: [4, 4.5, 3]
    },
    poster: require('../assets/posters/8418c5c56ab0f368d0c40974af04f12d.png'),
    marked: false
  },
  {
    id: 12,
    title: 'Mars Attaque',
    subTitle: null,
    genre: 'Comédie',
    abstract:
      'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque voluptatem adipisci perferendis eligendi, maxime cum reprehenderit placeat cupiditate aliquam amet veritatis dolorem ipsa alias deleniti et quam illum, sed a.',
    rate: {
      press: [3.5, 4, 3],
      spectator: [4, 4.5, 3]
    },
    poster: require('../assets/posters/08828814e48fd25eb17d8dcf48f2955d.png'),
    marked: false
  },
  {
    id: 13,
    title: 'Godzilla',
    subTitle: null,
    genre: 'Action',
    abstract:
      'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque voluptatem adipisci perferendis eligendi, maxime cum reprehenderit placeat cupiditate aliquam amet veritatis dolorem ipsa alias deleniti et quam illum, sed a.',
    rate: {
      press: [3.5, 4, 3],
      spectator: [4, 4.5, 3]
    },
    poster: require('../assets/posters/a5f131470b3590b9988779297b91576b.png'),
    marked: false
  },
  {
    id: 14,
    title: 'Avengers',
    subTitle: 'Infinity Wars',
    genre: 'Action',
    abstract:
      'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque voluptatem adipisci perferendis eligendi, maxime cum reprehenderit placeat cupiditate aliquam amet veritatis dolorem ipsa alias deleniti et quam illum, sed a.',
    rate: {
      press: [3.5, 4, 3],
      spectator: [4, 4.5, 3]
    },
    poster: require('../assets/posters/c78a46cf50680dfec32976673aabd539.png'),
    marked: false
  },
  {
    id: 15,
    title: 'Gremlins',
    subTitle: null,
    genre: 'Fantaisie',
    abstract:
      'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque voluptatem adipisci perferendis eligendi, maxime cum reprehenderit placeat cupiditate aliquam amet veritatis dolorem ipsa alias deleniti et quam illum, sed a.',
    rate: {
      press: [3.5, 4, 3],
      spectator: [4, 4.5, 3]
    },
    poster: require('../assets/posters/d5a06a22c9a32299d842c1ae7a8a9a63.png'),
    marked: false
  },
  {
    id: 16,
    title: 'Annihilation',
    subTitle: null,
    genre: 'Science Fiction',
    abstract:
      'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque voluptatem adipisci perferendis eligendi, maxime cum reprehenderit placeat cupiditate aliquam amet veritatis dolorem ipsa alias deleniti et quam illum, sed a.',
    rate: {
      press: [3.5, 4, 3],
      spectator: [4, 4.5, 3]
    },
    poster: require('../assets/posters/d35bea3435ac76ad8bc09d90beb1282b.png'),
    marked: false
  },
  {
    id: 17,
    title: 'Harry Potter et la Chambre des Secrets',
    subTitle: null,
    genre: 'Fantastique',
    abstract:
      'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque voluptatem adipisci perferendis eligendi, maxime cum reprehenderit placeat cupiditate aliquam amet veritatis dolorem ipsa alias deleniti et quam illum, sed a.',
    rate: {
      press: [3.5, 4, 3],
      spectator: [4, 4.5, 3]
    },
    poster: require('../assets/posters/e0447798de5fe4f295ee445545db4e38.png'),
    marked: false
  },
  {
    id: 18,
    title: 'The End Of the Fucking World',
    subTitle: null,
    genre: 'Dingue',
    abstract:
      'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque voluptatem adipisci perferendis eligendi, maxime cum reprehenderit placeat cupiditate aliquam amet veritatis dolorem ipsa alias deleniti et quam illum, sed a.',
    rate: {
      press: [3.5, 4, 3],
      spectator: [4, 4.5, 3]
    },
    poster: require('../assets/posters/eb8383b1386e1111ec620facb94931a6.png'),
    marked: false
  },
  {
    id: 19,
    title: 'Strangers Things',
    subTitle: null,
    genre: 'Fantastique',
    abstract:
      'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque voluptatem adipisci perferendis eligendi, maxime cum reprehenderit placeat cupiditate aliquam amet veritatis dolorem ipsa alias deleniti et quam illum, sed a.',
    rate: {
      press: [3.5, 4, 3],
      spectator: [4, 4.5, 3]
    },
    poster: require('../assets/posters/f242280fb41a6ee999c141694569e762.png'),
    marked: false
  },
  {
    id: 20,
    title: "Harry Potter et le Prisonnier d'Azkaban",
    subTitle: null,
    genre: 'Fantastique',
    abstract:
      'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque voluptatem adipisci perferendis eligendi, maxime cum reprehenderit placeat cupiditate aliquam amet veritatis dolorem ipsa alias deleniti et quam illum, sed a.',
    rate: {
      press: [3.5, 4, 3],
      spectator: [4, 4.5, 3]
    },
    poster: require('../assets/posters/fc084cd59ad3bc7fe9b0d742f92c75ab.png'),
    marked: false
  },
  {
    id: 21,
    title: '13 Reasons Why',
    subTitle: null,
    genre: 'Drame',
    abstract:
      'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eaque voluptatem adipisci perferendis eligendi, maxime cum reprehenderit placeat cupiditate aliquam amet veritatis dolorem ipsa alias deleniti et quam illum, sed a.',
    rate: {
      press: [3.5, 4, 3],
      spectator: [4, 4.5, 3]
    },
    poster: require('../assets/posters/7e22a616bd3c8ba56116c72313cad02b.png'),
    marked: false
  }
];

export default data;
