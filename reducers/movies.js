import data from '../constants/data';

export default function(state = { data: data }, action) {
  if (action.type === 'TOGGLE_MARKED_MOVIE') {
    return { data: action.payload };
  }

  return state;
}
