import React from 'react';
import { View, StyleSheet } from 'react-native';
import theme from '../constants/theme';
import SearchIcon from '../assets/icons/search.svg';
import BurgerIcon from '../assets/icons/format_align_left.svg';

const Header = () => {
  return (
    <View style={styles.header}>
      <BurgerIcon style={{ color: theme.colors.white }} />
      <SearchIcon style={{ color: theme.colors.white }} />
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 70,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    paddingHorizontal: 35
  }
});

export default Header;
