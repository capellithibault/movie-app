import React from 'react';
import StarHalfIcon from '../assets/icons/star_half.svg';
import StarIcon from '../assets/icons/star.svg';
import StarBorderIcon from '../assets/icons/star_border.svg';

const Rating = ({ average }) => {
  const limit = Math.round(Number.isInteger(average) ? average : average++);
  const isInteger = Number.isInteger(average);
  let result = [];
  for (let index = 0; index < 5; index++) {
    if (index === limit - 1 && !isInteger) {
      result.push(
        <StarHalfIcon width={15.26} height={15.26} key={index} color="#FFF" />
      );
    } else if (index < limit) {
      result.push(
        <StarIcon width={15.26} height={15.26} key={index} color="#FFF" />
      );
    } else {
      result.push(
        <StarBorderIcon width={15.26} height={15.26} key={index} color="#FFF" />
      );
    }
  }
  return result;
};

export default Rating;
