import React from 'react';
import {
  View,
  ImageBackground,
  Image,
  Dimensions,
  StyleSheet
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

let win = Dimensions.get('window');

const Cover = ({ children, src, containerHeight }) => {
  let { width } = Image.resolveAssetSource(src);
  let { height } = Image.resolveAssetSource(src);
  let newHeight = (win.width / width) * height;
  return (
    <>
      <View
        style={{ ...styles.container, height: win.height * containerHeight }}
      >
        <ImageBackground
          source={src}
          style={{ ...styles.image, height: newHeight, width: win.width }}
          resizeMode="cover"
        >
          <LinearGradient
            colors={[
              'rgba(8, 11, 24, 0.1)',
              'rgba(8, 11, 24, 0.6)',
              'rgba(8, 11, 24, 1)'
            ]}
            locations={[0.1, 0.6, 1]}
            style={styles.gradient}
          />
          {children}
        </ImageBackground>
      </View>
    </>
  );
};
const styles = StyleSheet.create({
  container: {
    overflow: 'hidden'
  },
  image: {
    flex: 1,
    justifyContent: 'flex-end'
  },
  gradient: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0
  },

  bgView: {
    height: 50,
    backgroundColor: 'rgba(8, 11, 24, 1)'
  }
});

export default Cover;
