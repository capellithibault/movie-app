import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import theme from '../constants/theme';
import { connect } from 'react-redux';
import { toggleMarkedMovie } from '../actions/index';
import BookmarkBorderIcon from '../assets/icons/bookmark_border.svg';
import BookmarkFilledIcon from '../assets/icons/bookmark.svg';
import ArrowBack from '../assets/icons/arrow_back.svg';
import ShareIcon from '../assets/icons/share.svg';

class MovieHeader extends React.Component {
  getMovieFromId = id => {
    result = {};
    for (let movie of this.props.movies) {
      if (movie.id === id) {
        result = movie;
      }
    }
    return result;
  };
  render() {
    const id = this.props.navigation.getParam('id');
    const { movies } = this.props;
    const currentMovie = this.getMovieFromId(id);

    // const TouchableBookmarkIcon = icon => (
    //   <TouchableOpacity

    //   >
    //     {icon}
    //   </TouchableOpacity>
    // );

    return (
      <View style={styles.header}>
        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
          <ArrowBack style={theme.icon} />
        </TouchableOpacity>
        <View style={styles.rightIcons}>
          <ShareIcon style={styles.shareIcon} />
          {currentMovie.marked ? (
            <BookmarkFilledIcon
              style={theme.icon}
              onPress={() => {
                this.props.toggleMarkedMovie([...movies], id);
                console.log(this.props.movies[id]);
              }}
            />
          ) : (
            <BookmarkBorderIcon
              style={theme.icon}
              onPress={() => {
                this.props.toggleMarkedMovie([...movies], id);
                console.log(this.props.movies[id]);
              }}
            />
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 70,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    paddingHorizontal: 35
  },
  rightIcons: {
    flexDirection: 'row'
  },
  shareIcon: {
    color: theme.colors.white,
    marginRight: 5
  }
});

mapStateToProps = store => {
  return {
    movies: store.movies.data
  };
};

const mapDispatchToProps = {
  toggleMarkedMovie
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MovieHeader);
