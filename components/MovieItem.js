import React from 'react';
import {
  View,
  StyleSheet,
  ImageBackground,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Text,
  Dimensions
} from 'react-native';
import theme from '../constants/theme';
import BookmarkBorderIcon from '../assets/icons/bookmark_border.svg';
import BookmarkFilledIcon from '../assets/icons/bookmark.svg';

let win = Dimensions.get('window');
const imageWidth = win.width * 0.35;
const ratio = 229 / 145;
const heightImage = imageWidth * ratio;

class MovieItem extends React.PureComponent {
  state = {
    active: false
  };

  _onPress = () => {
    this.props.onPressItem(this.props.id);
  };

  render() {
    console.log(this.state.active);
    const { poster, title, marked, navigate, id, abstract, genre } = this.props;
    const BookmarkIcon = () => {
      const icon = marked ? (
        <BookmarkFilledIcon color={theme.colors.white} />
      ) : (
        <BookmarkBorderIcon color={theme.colors.white} />
      );
      IconWrapper = icon => (
        <TouchableOpacity onPress={this._onPress}>{icon}</TouchableOpacity>
      );
      return IconWrapper(icon);
    };

    return (
      <View style={styles.movieItem}>
        <TouchableWithoutFeedback
          onPress={() => navigate('MovieDetails', { id, marked })}
          onPressIn={() => this.setState({ active: true })}
          onPressOut={() => this.setState({ active: false })}
        >
          <View style={styles.posterContainer}>
            <ImageBackground style={styles.poster} source={poster}>
              {this.state.active && (
                <View style={styles.activeBackground}>
                  <Text
                    style={{ ...theme.text, fontSize: 8, marginBottom: 10 }}
                  >
                    De Joe Russo, Anthony Russo
                  </Text>
                  <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                    <Text style={{ ...theme.text, fontSize: 8, flex: 1 }}>
                      {genre}
                    </Text>
                    <Text style={{ ...theme.text, fontSize: 8, flex: 2 }}>
                      {'3h01'}
                    </Text>
                  </View>
                  <Text style={{ ...theme.text, fontSize: 11 }}>
                    {abstract.slice(0, 100)} ...
                  </Text>
                </View>
              )}
            </ImageBackground>
          </View>
        </TouchableWithoutFeedback>
        <View style={styles.titleContainer}>
          <Text numberOfLines={1} style={[theme.text, styles.movieTitle]}>
            {title}
          </Text>
          <BookmarkIcon />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%'
  },
  movieTitle: {
    fontSize: 13,
    width: '80%'
  },
  movieItem: {
    width: imageWidth,
    maxWidth: 145,
    marginBottom: 26.7
  },
  posterContainer: {
    width: '100%',
    height: heightImage,
    maxHeight: 229,
    marginBottom: 11.42
  },
  poster: {
    width: '100%',
    height: '100%'
  },
  activeBackground: {
    backgroundColor: theme.colors.dark_blue,
    height: '100%',
    borderRadius: 10,
    opacity: 0.9,
    padding: 15
  }
});

export default MovieItem;
