import React from 'react';
import { connect } from 'react-redux';
import { toggleMarkedMovie } from '../actions/index';
import { View, StyleSheet, FlatList, Text } from 'react-native';
import theme from '../constants/theme';
import MovieItem from './MovieItem';

class MovieList extends React.Component {
  _keyExtractor = ({ id }) => id;

  _onPressItem = id => {
    this.props.toggleMarkedMovie([...this.props.movies], id);
  };

  _renderItem = ({ item }) => (
    <MovieItem
      id={item.id}
      onPressItem={this._onPressItem}
      marked={item.marked}
      title={item.title}
      poster={item.poster}
      abstract={item.abstract}
      genre={item.genre}
      navigate={this.props.navigation.navigate}
    />
  );

  getData = () => {
    if (this.props.context === 'Home') {
      return this.props.movies;
    } else if (this.props.context === 'Selection') {
      let array = [];
      for (let movie of this.props.movies) {
        if (movie.marked === true) {
          array.push(movie);
        }
      }
      return array;
    }
  };

  render() {
    return (
      <View style={[styles.listContainer, this.props.style]}>
        {this.props.title && (
          <Text style={{ ...theme.text, ...styles.listTitle }}>
            {this.props.title}
          </Text>
        )}
        <FlatList
          data={this.getData()}
          extraData={this.getData()}
          keyExtractor={this._keyExtractor}
          numColumns={2}
          renderItem={this._renderItem}
          columnWrapperStyle={styles.columnWrapper}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  listContainer: {
    paddingHorizontal: '9%',
    paddingTop: 10
  },
  listTitle: {
    marginBottom: 26,
    fontSize: 15
  },
  columnWrapper: {
    flex: 1,
    justifyContent: 'space-between'
  }
});

mapStateToProps = store => {
  return {
    movies: store.movies.data
  };
};

const mapDispatchToProps = {
  toggleMarkedMovie
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MovieList);
