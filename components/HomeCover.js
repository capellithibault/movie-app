import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import theme from '../constants/theme';
import Cover from './Cover';

const HomeCover = ({ src, containerHeight }) => {
  return (
    <Cover src={src} containerHeight={containerHeight}>
      <View style={styles.content}>
        <Text style={{ ...theme.text, ...styles.title }}>Godzilla</Text>
        <Text style={{ ...theme.text, ...styles.subTitle }}>
          Le Roi des Monstres
        </Text>
      </View>
    </Cover>
  );
};

const styles = StyleSheet.create({
  content: {
    marginBottom: 50
  },
  title: {
    fontSize: 59,
    fontWeight: 'bold',
    textAlign: 'center',
    fontFamily: 'Montserrat-Regular'
  },
  subTitle: {
    fontSize: 18,
    textAlign: 'center'
  }
});

export default HomeCover;
