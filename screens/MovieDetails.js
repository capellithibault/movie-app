import React from 'react';
import theme from '../constants/theme';
import MovieHeader from '../components/MovieHeader';
import Rating from '../components/Rating';
import Cover from '../components/Cover';
import { connect } from 'react-redux';
import {
  ScrollView,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Text
} from 'react-native';

class MovieDetails extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return { header: () => <MovieHeader navigation={navigation} /> };
  };

  getMovieFromId = () => {
    const { movies } = this.props;
    const id = this.props.navigation.getParam('id');
    currentMovie = {};
    for (let movie of movies) {
      if (movie.id === id) {
        currentMovie = movie;
      }
    }
    return currentMovie;
  };

  getAverageRate = rates => {
    reducer = (accumulator, currentValue) => accumulator + currentValue;
    const sum = rates.reduce(reducer);
    let average = Math.round((sum / rates.length) * 10) / 10;
    let averageArray = average.toString().split('.');
    let a = parseInt(averageArray[0]);
    let b = 10 - parseInt(averageArray[1]);
    let rounded = 0;
    if (b < 2.5) {
      rounded = a + 1;
    } else if (2 < 7.5) {
      rounded = a + 0.5;
    } else {
      rounded = a;
    }
    return [average, rounded];
  };

  render() {
    const {
      title,
      poster,
      rate,
      subTitle,
      genre,
      abstract
    } = this.getMovieFromId();
    const src = require('../assets/defaultImages/6ddd55e444d635ee97ea093ed2104443.png');

    const RatingContainer = ({ label, average, reviewsNumber }) => (
      <View style={{ marginBottom: 18 }}>
        <Text style={theme.text}>{label}</Text>
        <View style={styles.rateContainer}>
          <View style={styles.starsContainer}>
            <Rating average={average[1]} />
          </View>
          <Text style={{ ...theme.text }}>{average[0]}</Text>
          <Text style={styles.reviewsNumber}>{`(${reviewsNumber} avis)`}</Text>
        </View>
      </View>
    );

    return (
      <ScrollView>
        <Cover src={src} containerHeight={0.5} />
        <View style={styles.content}>
          <View style={styles.movieFeatures}>
            <View
              style={{
                flex: 1,
                overflow: 'hidden',
                maxHeight: 229,
                marginRight: 15
              }}
            >
              <Image
                style={{ maxWidth: 145, maxHeight: '100%' }}
                source={poster}
              />
            </View>

            <View style={{ flex: 1 }}>
              <Text style={[theme.text, styles.movieTitle]}>
                {title} {subTitle && `, ${subTitle}`}
              </Text>

              <View style={styles.features1}>
                <View style={styles.genreContainer}>
                  <Text style={[theme.text, styles.genre]}>{genre}</Text>
                </View>
                <Text style={{ ...theme.text, fontSize: 11 }}>{'3h01'}</Text>
              </View>

              <View>
                <Text style={[theme.text, styles.scriptwriter]}>
                  De Joe Russo, Anthony Russo
                </Text>
              </View>

              <RatingContainer
                label="Presse"
                average={this.getAverageRate(rate.press)}
                reviewsNumber={rate.press.length.toString()}
              />

              <RatingContainer
                label="Spectateurs"
                average={this.getAverageRate(rate.spectator)}
                reviewsNumber={rate.spectator.length.toString()}
              />
            </View>
          </View>
          <Text style={[theme.text, styles.abstract]}>{abstract}</Text>
          <TouchableOpacity style={styles.button}>
            <Text style={[theme.text, styles.buttonText]}>Lecture</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    position: 'relative',
    top: -90,
    marginHorizontal: '9%'
  },
  movieTitle: {
    fontSize: 16,
    marginBottom: 11
  },
  rateContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  starsContainer: {
    flexDirection: 'row'
  },
  reviewsNumber: {
    fontSize: 8,
    color: theme.colors.grey
  },
  movieFeatures: {
    flexDirection: 'row',
    marginBottom: 15
  },
  features1: {
    flexDirection: 'row',
    marginBottom: 11
  },
  genreContainer: {
    borderWidth: 1,
    borderColor: theme.colors.white,
    borderRadius: 3,
    marginRight: 13
  },
  genre: {
    fontSize: 8,
    padding: 3
  },
  scriptwriter: {
    fontSize: 8,
    marginBottom: 21
  },
  abstract: {
    fontSize: 11,
    lineHeight: 20,
    marginBottom: 32
  },
  button: {
    height: 46,
    backgroundColor: theme.colors.purple,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    fontSize: 16
  }
});

mapStateToProps = store => {
  return {
    movies: store.movies.data
  };
};

export default connect(
  mapStateToProps,
  undefined
)(MovieDetails);
