import React, { Component } from 'react';
import theme from '../constants/theme';
import {
  ScrollView,
  View,
  TouchableOpacity,
  Text,
  StyleSheet
} from 'react-native';
import { createStackNavigator } from 'react-navigation';
import MovieDetails from './MovieDetails';
import Header from '../components/Header';
import HomeCover from '../components/HomeCover';
import MovieList from '../components/MovieList';
import HomeIcon from '../assets/icons/home.svg';

class Home extends Component {
  static navigationOptions = {
    header: Header
  };

  render() {
    console.log(this.props.navigation.state.routeName);
    let src = require('../assets/posters/7e161b5625b4eb6a8c23c92da81c5657.png');
    const MySelectionButton = ({ navigate }) => (
      <View>
        <TouchableOpacity
          onPress={() => navigate('Selection')}
          style={styles.button}
        >
          <Text style={styles.buttonText}>+ Ma sélection</Text>
        </TouchableOpacity>
      </View>
    );

    return (
      <ScrollView>
        <HomeCover src={src} containerHeight={0.65} />
        <MySelectionButton navigate={this.props.navigation.navigate} />
        <MovieList
          context="Home"
          navigation={this.props.navigation}
          title="La sélection Wemap"
        />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    transform: [{ translateY: -18 }],
    height: 36,
    borderWidth: 1,
    borderColor: theme.colors.yellow,
    borderRadius: 50,
    justifyContent: 'center',
    alignSelf: 'center'
  },
  buttonText: {
    color: theme.colors.yellow,
    paddingHorizontal: 20,
    zIndex: 2
  }
});

const StackNavigator = createStackNavigator(
  {
    Home: {
      screen: Home
    },
    MovieDetails: {
      screen: MovieDetails
    }
  },
  {
    cardStyle: {
      backgroundColor: theme.colors.dark_blue
    }
  }
);

StackNavigator.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
    tabBarIcon: ({ tintColor }) => {
      return <HomeIcon color={tintColor} />;
    }
  };
};

export default StackNavigator;
