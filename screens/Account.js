import React, { Component } from 'react';
import { View, Text } from 'react-native';
import AccountIcon from '../assets/icons/account_circle.svg';

class Account extends Component {
  render() {
    return (
      <View>
        <Text>Account</Text>
      </View>
    );
  }
}

Account.navigationOptions = {
  tabBarIcon: ({ tintColor }) => {
    return <AccountIcon color={tintColor} />;
  }
};

export default Account;
