import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import MovieDetails from './MovieDetails';
import Header from '../components/Header';
import MovieList from '../components/MovieList';
import theme from '../constants/theme';
import StarIcon from '../assets/icons/star.svg';

class Selection extends Component {
  static navigationOptions = {
    header: Header
  };

  render() {
    return (
      <ScrollView>
        <MovieList
          context="Selection"
          navigation={this.props.navigation}
          title="Ma sélection"
          style={{ paddingTop: 90 }}
        />
      </ScrollView>
    );
  }
}

const StackNavigator = createStackNavigator(
  {
    Selection: {
      screen: Selection
    },
    MovieDetails: {
      screen: MovieDetails
    }
  },
  {
    cardStyle: {
      backgroundColor: theme.colors.dark_blue
    }
  }
);

StackNavigator.navigationOptions = ({ navigation }) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }

  return {
    tabBarVisible,
    tabBarIcon: ({ tintColor }) => {
      return <StarIcon color={tintColor} />;
    }
  };
};

export default StackNavigator;
