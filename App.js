import React from 'react';
import { StatusBar } from 'react-native';
import store from './store';
import { Provider } from 'react-redux';
import { createAppContainer, createBottomTabNavigator } from 'react-navigation';
import theme from './constants/theme';
import Home from './screens/Home';
import Selection from './screens/Selection';
import Account from './screens/Account';

const TabNavigator = createBottomTabNavigator(
  {
    Home,
    Selection,
    Account
  },
  {
    tabBarOptions: {
      showLabel: true,
      activeTintColor: theme.colors.white,
      inactiveTintColor: theme.colors.grey,
      style: {
        backgroundColor: theme.colors.blue_grey,
        height: 60,
        alignItems: 'center',
        justifyContent: 'center'
      },
      tabStyle: {
        paddingVertical: 5
      },
      safeAreaInset: { bottom: 'never', top: 'never' }
    }
  }
);

const AppContainer = createAppContainer(TabNavigator);

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <StatusBar hidden />
        <AppContainer />
      </Provider>
    );
  }
}
