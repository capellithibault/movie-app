// export const getMovies = () => dispatch => {
//   dispatch({ type: 'INITIALIZE_MOVIES', payload: data });
// };

export const toggleMarkedMovie = (data, id) => dispatch => {
  for (let [index, movie] of data.entries()) {
    if (movie.id === id) {
      data[index].marked = !data[index].marked;
    }
  }
  dispatch({ type: 'TOGGLE_MARKED_MOVIE', payload: data });
};
